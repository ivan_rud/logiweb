package com.ruddi.logiweb.service;

import com.ruddi.logiweb.bl.GraphService;
import com.ruddi.logiweb.dao.api.OrderDao;
import com.ruddi.logiweb.dto.OrderDto;
import com.ruddi.logiweb.model.Order;
import com.ruddi.logiweb.service.api.CountryService;
import com.ruddi.logiweb.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {
    @Mock
    OrderDao dao;

    @Mock
    ModelMapper mapper;

    @Mock
    GraphService graphService;

    @Mock
    CountryService countryService;

    @InjectMocks
    OrderServiceImpl service;


    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void removeTest() {
        Order order = new Order();
        order.setCargos(new ArrayList<>());
        order.setDrivers(new ArrayList<>());

        Mockito.when(dao.find(2)).thenReturn(order);
        Mockito.doNothing().when(dao).remove(2);

        Assertions.assertDoesNotThrow(() -> service.remove(2));
    }
}
